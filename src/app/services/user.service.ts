import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { of } from 'rxjs';

import { User } from '../models/User';

@Injectable({
  providedIn: 'root'
})
export class UserService {
   users: User[];
   data: Observable<User[]>;

  constructor() { 
     this.users = [
      {
         firstName: 'John',
         lastName: 'Doe',
         email: 'joh.doe@yahoo.com',
         isActive: true,
         registered: new Date('01/03/2020 08:30'),
         hide: true
      },
      {
         firstName: 'John',
         lastName: 'Sandford',
         email: 'john.sandford@brains.com',
         isActive: false,
         registered: new Date('06/07/2021 08:30'),
         hide: true
      },
      {
         firstName: 'Wilbur',
         lastName: 'Smith',
         email: 'wilbur.smith@haggard.net',
         isActive: true,
         registered: new Date('12/03/2019 08:30'),
         hide: true
      
      }

     ];
   }


   getUsers(): Observable<User[]> {
      return of(this.users);
   }

   addUser(user: User){
      this.users.unshift(user);
   }

}
