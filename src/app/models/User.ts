import { Address } from "src/app/models/Address";

export interface User {
   firstName: string;
   lastName: string;
   email: string;
   isActive?: boolean;
   registered?: any;
   hide?: boolean;
}