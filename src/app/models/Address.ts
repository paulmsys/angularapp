export interface Address {
   street: string,
   city?: String,
   state?: string
}